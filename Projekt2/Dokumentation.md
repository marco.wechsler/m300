# Projekt 2
# Inhaltsverzeichnis
[TOC]
# Auftrag
## Anforderungen analysieren
Als erstes habe ich den Auftrag sorgfältig durchgelesen und versucht, die Anforderungen zu verstehen. Ich muss eine E-Commerce-Lösung entwickeln, ähnlich wie im vorherigen Projekt, jedoch dieses Mal in der Cloud und nicht mehr lokal auf meinem Laptop. Mein Ziel ist es, die gleiche E-Commerce-Anwendung in der Cloud umzusetzen. Nachdem ich die verschiedenen Cloud-Plattformen in Betracht gezogen habe, habe ich mich für Microsoft Azure entschieden.

## Planen und Entscheiden
Ich habe hier die gleichen Microservices und die gleiche E-Commerce-Lösung wie bei Projekt 1 genommen. Danach habe ich mich für eine Cloud-Lösung entschieden. Bei mir wurde es Azure Cloud. Anschliessend habe ich einen Account erstellt und dann war ich startklar. Ich habe mich anhand dieses [YT-Video](https://www.youtube.com/watch?v=iQN9jFqstt8) orientiert.

## Infrastruktur für die Microservices bereitstellen
Als erstes habe ich ein storage in Azure erstellt.

![storage erstellen](/Projekt2/Bilder/a.png){ width=50% }

So sieht dass dan aus hier habe ich einfach die location und den namen angepasst den rest kann man sein lassen.

![storage erstellen](/Projekt2/Bilder/aaa.png){ width=50% }

Danach ging ich zu statische webseite und habe dort einen endpoiint erstellt.

![storage erstellen](/Projekt2/Bilder/aa.png){ width=50% }

Ich bin dann zu Container Web gegangen, denn es schon automatisch erstellt hatte und habe die Files von Projekt1 hochgeladen und einen Container erstellt.

![storage erstellen](/Projekt2/Bilder/aaaa.png){ width=50% }

## Entwicklung der Microservices
Hier habe ich einfach dasselbe wie bei [Projekt1](https://gitlab.com/marco.wechsler/m300/-/tree/main/Projekt1/M300%20Umgebung) genommen.
## Web-Applikation und APIs entwickeln
Hier habe ich den gleichen [API](https://gitlab.com/marco.wechsler/m300/-/blob/main/Projekt1/Dokumentation.md) genommen wie bei projekt1.
## Monitoring und Überwachung
In Azure hat es ein abschnitt der Überwachungen heisst.

![storage erstellen](/Projekt2/Bilder/aaaaaa.png){ width=50% }

Hier kann man dann eine Warnung erstellen und hat so die Überwachung über den Container, wenn zb ein container überlastet ist.

![storage erstellen](/Projekt2/Bilder/aaaaaaa.png){ width=50% }

## Testing
Zum Schluss habe ich dann das Ganze noch getest und wie man sieht hatte es funktioniert.

![storage erstellen](/Projekt2/Bilder/aaaaa.png){ width=50% }

## image registry

In Azure kann man hier ein einfaches image registy erstellen.

![storage erstellen](/Projekt2/Bilder/b.png){ width=50% }

Hier wird es dann überprüft nach dem erstellen

![storage erstellen](/Projekt2/Bilder/bb.png){ width=50% }

Wie man sieht wurde es dann erstellt hier kann man dann die container images hoch pushen

![storage erstellen](/Projekt2/Bilder/bbb.png){ width=50% }








