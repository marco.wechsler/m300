const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const PORT = 3000;

// Middleware für das Parsen von JSON-Daten
app.use(bodyParser.json());

// Endpunkt zum Speichern der Umfrageergebnisse
app.post('/survey', (req, res) => {
    const { choice } = req.body;

    // Daten in einer Datei speichern (als einfaches Beispiel)
    fs.appendFile('survey_results.txt', choice + '\n', (err) => {
        if (err) {
            console.error('Fehler beim Speichern der Umfrageergebnisse:', err);
            res.status(500).send('Interner Serverfehler');
        } else {
            console.log('Umfrageergebnis gespeichert:', choice);
            res.status(200).send('Umfrageergebnis erfolgreich gespeichert');
        }
    });
});

// Server starten
app.listen(PORT, () => {
    console.log(`Server läuft auf Port ${PORT}`);
});
