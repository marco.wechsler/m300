# Projekt
# Inhaltsverzeichnis
[TOC]
# Auftrag
## Anforderungen analysieren: 
Als erstes habe ich den Auftrag durchgelesen und mal die Grundstruktur der Dokumentation erstellt. Danach habe ich mir überlegt was wird alles benötigt. Ich brauche eine kleine Webseite wo ich eine Basicapplikation abrufen kann die mit einem API funktionieren soll. Jetzt brauchte ich nur noch eine Idee was ich machen möchte. Nachdem habe ich darüber nachgedacht, welche Informationen ich regelmässig benötige und einsehen möchte, kam mir dann die Idee, dass Fussballinformationen eine hohe Priorität haben. Deshalb entschied ich mich, diese Funktion zu entwickeln. Als nächstes begann ich damit, nach einer passenden API zu suchen. Durch ein wenig Recherche im Internet stieß ich schliesslich auf [ESPN](https://smarthomeyourself.de/wiki/homeassistant/teamtracker-fussball-bundesliga-tabelle-in-home-assistant/) als eine vielversprechende Quelle. Später wurde mir dann aber klar das dies nur mit einem Smarthomeprodukt funktionieren kann. Dasheisst ich musst wieder von vorn Anfangen.
<br>
Danach bin ich dann auf [Openliga](https://www.openligadb.de/) gestossen. Ich versuchte es dann auch mit Openliag.

## Planen und Entscheiden:
Ich skizzierte ich einen groben Plan, wie ich mir die Umsetzung dieser Idee vorstellte. 

![Übersicht](/Projekt1/Bilder/Übersichtprojekt.jpg){ width=50% }

Ich habe einen Microservice für die Tabelle und für die Resultate geplannt. Dazu werde ich noch eine eine Datenbank anschliessen, damit man sich einloggen kann. Ich habe mich für die Technologie **docker-compose** entscheiden.<br>
Hier habe ich hier noch einen genaueren Plan von meiner Applikation erstellt:

![Übersicht](/Projekt1/Bilder/neuübersicht.jpg){ width=50% }

Da dieser ESPN API nur mit Smarthomeprodukten funktioniert musste ich wieder eine neue API Lösung finden. Der Rest bleibt gleich. <br> Als im Plenum einen goben Plan gezeigt wurde wie so ein Projekt aussehen könnte wurde mir erst klar, wie genau das dann schlussendlich aussehen sollte. Das hat mir stark geholfen bei der Umsetzung

## Infrastruktur für die Microservices bereitstellen: 
Zuerst versuchte ich docker-compose zu installieren ich habe dann aber schnell gemerkt das ich keine Umgebung hatte wo ich es laufen lassen konnte. Im letzten Modul stand uns eine VM zur Verfügung und wir konnten dort docker-compose installieren, deshalb hatte ich es hier auch nochmal versucht. <br>
Als ersten Schritt habe ich docker-compose installiert:
```Shell
      $ sudo apt install docker-compose
 ```
 So konnte ich überpüfen ob docker-compose erfolgreich installiert wurde:
 ```Shell
      $ docker-compose version
 ```
Ich habe dann Thajuran wie er dies gelöst habe. Er empfohl mir dann docker Desktop. Ich habe mich dann auch dazu entschlossen docker Desktop herunter zu laden. Auf dieser Umgebund kann ich dann die docker Container laufen lassen. Hier habe ich [docker Desktop](https://www.docker.com/products/docker-desktop/) heruntergeladen.

Wenn ich docker desktop installiert habe kann ich ins Verzeichnis wechseln wo die entsprechenden Dokumente liegen. Bei mir sind die entsprechenden Dokumente unter dem Verzeichnis [Umgebung](https://gitlab.com/marco.wechsler/m300/-/tree/main/Projekt1/M300%20Umgebung?ref_type=heads) 
und mit:
 ```Shell
      docker compose up
 ```
Danach sind die Container gestartet und man kann mit localhost:8080 darauf zugreifen. 8080 ist der port der definiert wurde im dockerfile.

## Entwicklung der Microservices:
So sieht mein docker-compose.yaml file aus:
 ```Shell
     version: '3'

     services:
     frontend:
     build: ./frontend
     ports:
          - "80:80"
     depends_on:
          - database
          - api

     api:
     build: ./api
     ports:
          - "5000:5000"
     depends_on:
          - database

     database:
     image: mysql:5.7
     environment:
          MYSQL_ROOT_PASSWORD: root_password
          MYSQL_DATABASE: football_results
          MYSQL_USER: user
          MYSQL_PASSWORD: password
     ports:
          - "3306:3306"
     volumes:
          - db_data:/var/lib/mysql

     volumes:
     db_data:
 ```

Entwicklung und Bereitstellung von einzelnen Microservices mit den ausgewählten Technologien (Docker, Kubernetes) für eine flexible und skalierbare Infrastruktur. Die Konfiguration und Setup der Microservices sollten klar und wenn möglich graphisch dargestellt werden.
## Web-Applikation und APIs entwickeln: 
So sieht meine index.php aus:
```Shell
     <!DOCTYPE html>
     <html lang="en">
     <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Football Results</title>
     </head>
     <body>
     <h1>Football Results</h1>
     <div id="results"></div>
     <script>
          fetch('http://api:5000/results')
               .then(response => response.json())
               .then(data => {
                    const resultsDiv = document.getElementById('results');
                    data.forEach(result => {
                         const p = document.createElement('p');
                         p.textContent = `${result.home_team} ${result.home_score} - ${result.away_score} ${result.away_team}`;
                         resultsDiv.appendChild(p);
                    });
               });
     </script>
     </body>
     </html>
 ```
Das ist das Dockerfile zu index.php:
 ```Shell
     # Frontend Dockerfile
     FROM php:7.4-apache
     COPY index.php /var/www/html/
 ```

So sieht mein api.py aus:
```Shell
     from flask import Flask, jsonify
     from flask_sqlalchemy import SQLAlchemy

     app = Flask(__name__)
     app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://user:password@database/football_results'
     db = SQLAlchemy(app)

     class Result(db.Model):
     id = db.Column(db.Integer, primary_key=True)
     home_team = db.Column(db.String(50))
     away_team = db.Column(db.String(50))
     home_score = db.Column(db.Integer)
     away_score = db.Column(db.Integer)

     @app.route('/results')
     def get_results():
     results = Result.query.all()
     return jsonify([{'home_team': result.home_team,
                         'away_team': result.away_team,
                         'home_score': result.home_score,
                         'away_score': result.away_score} for result in results])

     if __name__ == '__main__':
     app.run(host='0.0.0.0', port=5000)
 ```
Das ist das Dockerfile zu api.py:
 ```Shell
     # API Dockerfile
     FROM python:3.9
     WORKDIR /app
     COPY api.py .
     RUN pip install flask flask_sqlalchemy pymysql
     CMD ["python", "api.py"]
     # Install required dependencies
     RUN pip install flask flask_sqlalchemy pymysql
     # Install MySQL client library
     RUN apt-get update && apt-get install -y default-libmysqlclient-dev
     # Install MySQLdb module
     RUN pip install mysqlclient
     CMD ["python", "api.py"]
 ```
## Fazit
Das Projekt begann mit großer Begeisterung, jedoch stellte sich heraus, dass ESPN nur mit Smart-Home-Produkten kompatibel war, was ich erst spät bemerkte. Dies war ein Rückschlag, der mich weit zurückwarf. Danach sah ich mich einem großen Berg an Arbeit gegenüber, der eher demotivierend als inspirierend war. Trotzdem habe ich mein Bestes gegeben, aber das Schreiben des Codes fiel mir immmer noch schwer. Letztendlich fühlte ich mich frustriert und versuchte, das Beste aus der Situation zu machen. Ich habe jetzt einfach mal so viel wie möglich abgeben.

